var webpack = require('webpack');

module.exports = {
    mode: 'production',
    watch: true,
    entry:
    {
        index : "./pub/js/index.jsx",
        editor: "./pub/js/editor.jsx",
        dashboard: "./pub/js/dashboard.jsx",
        docs: "./pub/js/docs.jsx"
    },
    output: {
        filename: "[name].bundle.js",
        path: __dirname + "/pub/js"
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            'process.env.BABEL_ENV': JSON.stringify(process.env.BABEL_ENV)
        }),
    ],

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
    },

    module: {
        rules: [
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
        ],
    },

    // modules: {
    //     resolve.fallback: { "path": false }
    // },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
    optimization: {
        minimize: true, // <---- disables uglify.
        // minimizer: [new UglifyJsPlugin()] if you want to customize it.
    }
};
