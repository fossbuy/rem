<html>
    <head>
        <@include header.html.tpl %>
    </head>
    <body>
        <div id="article-container">
            <div id="article-meta">
                <%= meta %>
            </div>
            <div id="article-content" oid=<%= (object->string oid) %>>
            </div>
        </div>
    </body>
    <@js docs.bundle.js %>
</html>
