<html>
    <head>
        <@include header.html.tpl %>
    </head>
    <body>
        <div id="root"></div>
    </body>
    <@js dashboard.bundle.js %>
</html>
