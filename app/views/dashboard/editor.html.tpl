<html>
    <head>
        <@include header.html.tpl %>
        <@css editor.css %>
        <@css ../../node_modules/simplemde/dist/simplemde.min.css %>
    </head>
    <body>
        <div class=".main-content">
            <div id="root"></div>
        </div>
    </body>
    <@js editor.bundle.js %>
</html>
