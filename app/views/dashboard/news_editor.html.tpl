<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
        <@css editor.css %>
        <@css ../../node_modules/simplemde/dist/simplemde.min.css %>
    </head>

    <body>
        <div id="editor_root"></div>
    </body>
    <@js editor.bundle.js %>
</html>
