;; Controller index definition of rem
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller index) ; DO NOT REMOVE THIS LINE!!!

(get "/" (lambda (rc) (redirect-to rc "/index/")))

(index-define
 "/"
 (lambda (rc)
   (view-render "index" (the-environment))))
