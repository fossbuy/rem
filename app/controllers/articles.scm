;; Controller articles definition of rem
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller articles) ; DO NOT REMOVE THIS LINE!!!

(articles-define
 "docs/:oid"
 (lambda (rc)
   (let ((path (format #f "docs/~a" (params rc "oid"))))
     (view-render "docs" (the-environment)))))
