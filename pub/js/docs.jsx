import React from 'react'
import ReactDOM from 'react-dom';
import { createMarkdown } from "safe-marked";

const markdown = createMarkdown();

class MarkdownViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: 'nothing'
        };
    }

    componentDidMount() {
        fetch('/' + this.props.oid + '/content.md')
            .then(res => res.text())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        data: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                })
    }

    render() {
        const { error, isLoaded, data } = this.state;
        let md = markdown(data);

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <p>123123</p>
                    <div dangerouslySetInnerHTML={{__html: md}} />
                </div>
            );
        }
    }
}

ReactDOM.render(<MarkdownViewer oid={article.getAttribute('oid')} />,
                document.getElementById('article-content'));
